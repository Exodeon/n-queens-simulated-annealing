# N queens by simulated annealing

A simulated annealing algorithm to solve the N-queens problems.

You change the value of `n` in the code by anything you want to solve any N-queens problem you want.

The output is a list of rows, assuming that there is one queen by column.