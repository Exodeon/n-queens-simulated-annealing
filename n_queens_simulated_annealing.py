import random
import math


def get_energy(board, n):
    """Compute the total energy of the board.

    We only have to check for diagonals because the way the board is made
    insure that there is only one queen per line and per column"""
    energy = 0
    # for each queen, we will check every next queen
    for i in range(n-1):
        # we can begin at i+1 because we already checked the previous
        for j in range(i+1, n):
            if abs(board[j] - board[i]) == j - i:
                energy += 1
    return energy


# parameters
n = 8
rate = 0.99
temperature = 1

board = list(range(1, n + 1))
current_energy = get_energy(board, n)
# in this case, we have a solution when energy = 0

iterations_number = 0
while current_energy > 0 and temperature > 0:
    # we choose 2 columns to swap
    columns = random.sample(list(range(n)), 2)
    # we copy the board and swap columns in order to compute the new energy
    new_board = board.copy()
    new_board[columns[0]], new_board[columns[1]] = new_board[columns[1]], new_board[columns[0]]
    new_energy = get_energy(new_board, n)
    delta_energy = new_energy - current_energy
    # we change the board if the board is better or with some chance
    if delta_energy < 0 or random.random() < math.exp(-delta_energy/temperature):
        board = new_board
        current_energy = new_energy
    # we decrease the temperature
    temperature = rate * temperature
    iterations_number += 1

print(board, "with energy", current_energy, "in", iterations_number, "iterations")
